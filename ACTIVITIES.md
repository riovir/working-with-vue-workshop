# Activities

Let's create a Joke rating app!

```bash
mkdir vue-workshop
cd vue-workshop
npm init @riovir/vue-app
npm i
npm i -D json-server
```

## Let's fire up a server

Hook up `json-server` with the dev environment.
- find out how
- using the knowledge that the `webpack-dev-server` is actually backed by `Express` wire it in.

## Dump the reviews in the screen

- decide what a review is (schema)
- create a `domain factory` -> `Review({ someReviewProp: 42, anotherOne: 'hello' })`
- hook it up to the API
- hook it up to the store
- dump on the screen

## Create a page dedicated for reviewing a random joke
- oops, the concept of a joke is just a `String` let's turn it into a `domain object` -> `Joke({ id, url, content })`
- actually, only load a random joke if the page doesn't get an id for one
- dump reviews on the page about the current joke

## Flesh out the GUI a bit

- List the TOP 10 reviewed jokes
- Add a random joke to the page
- Allow reviewing it there
