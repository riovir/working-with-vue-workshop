// The 'preview.js' is responsible to setting up how stories get rendered
// Import the same styles the app normally uses
import 'src/style/main.scss';
// Make sure vue is set up the same way as usual
import 'src/vue-setup';

import { addParameters } from '@storybook/vue';

// Allow categorizing stories. Eg. a story called "widgets/td-icon"
// will end up in the WIDGETS category
addParameters({
	options: {
		showRoots: true,
	},
});
