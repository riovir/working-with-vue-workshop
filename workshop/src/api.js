import axios from 'axios';
import { Review } from './domain/review';

export function Api({ get, _post } = Axios()) {
	return {
		async getChuckNorrisJoke() {
			const { data } = await get('joke-api/jokes/random?escape=javascript');
			return data.value.joke;
		},
		async fetchReviews() {
			const { data } = await get('api/reviews');
			return data.map(Review);
		},
	};
}

// Use to set convenient defaults.
// * It's recommended that a request starts with a '/'
// * If it starts with http(s):// then its either unnecessary or triggers cross-origin requests.
// * If it leaves the '/' out then it will be relative to the current path, which requests sensitive to navigation.
function Axios() {
	return axios.create({ baseURL: process.env.PUBLIC_PATH });
}
