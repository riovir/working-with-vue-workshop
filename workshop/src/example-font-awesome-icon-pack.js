/* eslint-disable max-len */

/* A Font Awesome 5 style icon looks like this. Icon packs tend to have a prefix to avoid colliding with each other. */
export const prefix = 'td';

export const tdExample = {
	prefix,
	iconName: 'example',
	icon: [
		/* width */ 32,
		/* height */ 32,
		/* ligatures */ [],
		/* unicode */ 'f005',
		/* SVG path */ 'M16 0c-8.837 0-16 7.163-16 16s7.163 16 16 16 16-7.163 16-16-7.163-16-16-16zM16 28c-6.627 0-12-5.373-12-12s5.373-12 12-12c6.627 0 12 5.373 12 12s-5.373 12-12 12z',
	],
};
