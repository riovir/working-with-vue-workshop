import Vuex from 'vuex';
import { Store } from 'src/store';
import TheMessage from './the-message';

export default { title: 'components/the-message' };

const DOES_TOO_MUCH = `
While it's possible to hook up Storybook with Vuex, it's not the best idea.
Instead components responsible for the "look and feel" should do just that,
moving into /widgets. There they can get a nice and simple Storybook page
each without this Vuex and network mocking nonsense.`;

export const shouldNotBeInStorybook = () => ({
	template: `
		<div>
			<section class="section has-text-danger">
				<p>
					Careful: Mixing app and UI components can get out of hand quickly.
				</p>
				<button @click="provokeMessage">Show message</button>
			</section>
			<the-message />
		</div>
	`,
	// Pass in a custom made store into the app component
	store: DummyStore(),
	components: { TheMessage },
	data: () => ({ active: true }),
	methods: {
		provokeMessage() {
			// Would also break if message handling changes in the store
			this.$store.commit('showMessage', DOES_TOO_MUCH);
		},
	},
});

function DummyStore() {
	// Build a store with a useless API
	const store = Store({ api: false });
	const actions = {
		...store.actions,
		// Doing nothing where networking would be needed
		loadJoke: () => {/* Do nothing */},
	};
	return new Vuex.Store({ ...store, actions });
}
