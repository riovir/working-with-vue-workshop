import './style/main.scss';

import Vue from 'vue';
import * as vueSetup from './vue-setup';
import { fetchTranslations } from './vue-setup';

fetchTranslations({ locale: 'hu-hu', languageCode: 'hu' });

new Vue({ el: '#app', render: h => h('router-view'), ...vueSetup });
