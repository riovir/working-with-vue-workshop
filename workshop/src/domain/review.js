export function Review({
	id = null,
	aboutUrl = null,
	rating = null,
	remark = '',
} = {}) {
	return { id, aboutUrl, rating, remark };
}

export function isGood({ rating }) {
	return 4 <= rating;
}
