// Used from Buefy, the Vue.js convenience over Bulma
export default { title: 'widgets/td-modal-card' };

// And entry can also be a full blown Vue component
// with data, methods, etc.
export const byDefault = () => ({
	template: `
		<div>
			<button @click="active = true">Show td-modal-card</button>

			<td-modal-card :active="active" @close="active = false">
				<template slot="title">Lorem ipsum</template>

				Ideally all components responsible for the <strong>look and feel</strong> are located
				separately from the <strong>app components</strong>. In this setup they are inside the
				<code>/widgets</code> and stay clear from concepts such as:
				<ul>
					<li class="has-text-weight-bold">- application logic and Vuex</li>
					<li class="has-text-weight-bold">- networking</li>
					<li class="is-italic">- arguably i18n and locales</li>
				</ul>

				<button slot="foot" class="button is-primary" @click="active = false">Primary action</button>
				<button slot="foot" class="button" @click="active = false">Cancel</button>
			</td-modal-card>
		</div>
	`,
	data: () => ({ active: false }),
});
