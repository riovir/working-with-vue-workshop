import TdIcon from './td-icon';
import TdModalCard from './td-modal-card';

// "Widgets" are just vue files that
// - are independent of the application shared state
// - are globally available in your app
// - are meant to be reusable
export default function install(Vue) {
	// This icon follows Bulma's expectation of Font Awesome icons
	Vue.component(TdIcon.name, TdIcon);
	// An example of Buefy's modal + Bulma modal card + vue-focus-lock
	Vue.component(TdModalCard.name, TdModalCard);
}
