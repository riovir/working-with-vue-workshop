export function Store({ api }) {
	const state = {
		counter: 0,
		message: null,
		reviews: [],
	};

	const getters = {
		isTired({ counter }) {
			return counter >= 5;
		},
	};

	const mutations = {
		incrementCounter(state) {
			state.counter++;
		},
		showMessage(state, message) {
			state.message = message;
		},
		setReviews(state, reviews) {
			state.reviews = reviews;
		},
	};

	const actions = {
		async loadReviews({ commit }) {
			commit('setReviews', await api.fetchReviews());
		},
		incrementCounter({ commit, getters }) {
			commit('incrementCounter');
			if (getters.isTired) {
				commit('showMessage', 'Getting tired with all this incrementing nonsense.');
			}
		},
		dismissMessage({ commit }) {
			commit('showMessage', null);
		},
		async loadJoke({ commit }) {
			commit('showMessage', await api.getChuckNorrisJoke());
			console.log(await api.fetchReviews());
		},
	};

	return { state, getters, mutations, actions };
}
