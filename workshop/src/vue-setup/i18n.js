import Vue from 'vue';
import VueI18n from 'vue-i18n';

import en from '../resources/en-us';

Vue.use(VueI18n);

export const i18n = new VueI18n({
	locale: 'en',
	fallbackLocale: 'en',
	messages: { en },
});

// Translations have the tendency to bloat up bundle size considerably. They can be lazy fetched once that happens.
export async function fetchTranslations({
	// Eg. "en-us"
	locale,
	// Eg. "en"
	languageCode = locale,
}) {
	// Example of dynamic imports by webpack. The "magic comment" is optional. Requires the @babel/plugin-syntax-dynamic-import plugin.
	const translation = await import(/* webpackChunkName: "lang-[request]" */ `src/resources/${locale}`);
	// In this example the translation module comes from a Webpack-loaded JSON file.
	// It's default export is the translations object.
	i18n.setLocaleMessage(languageCode, translation.default);
}
