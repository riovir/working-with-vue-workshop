import Vue from 'vue';
import VueRouter from 'vue-router';
import TheFrame from 'src/the-frame';

Vue.use(VueRouter);

const routes = [
	{ path: '/', redirect: { name: 'the-reviews' } },
	{ path: '/', component: TheFrame, children: [
		{ path: 'reviews', name: 'the-reviews', component: () => import(/* webpackChunkName: "the-reviews" */ 'src/pages/the-reviews') },
		{ path: 'app', name: 'the-app', component: () => import(/* webpackChunkName: "the-app" */ 'src/pages/the-app') },
	] },
];

export const router = new VueRouter({
	routes,
	mode: 'history',
	base: process.env.PUBLIC_PATH,
});
