const { resolve } = require('path');
const jsonServer = require('json-server');

const DB_PATH = resolve(__dirname, '../dev/db.json');

// The Webpack Dev Server uses Express under the hood
module.exports = function DevServerConfig({ publicPath = '/' } = {}) {
	return {
		historyApiFallback: { index: publicPath },
		publicPath,
		compress: true,
		hot: true,
		port: 3000,
		// Using an external API could be a mock-server or the real thing
		proxy: proxy({
			path: `${publicPath}joke-api`,
			target: 'http://api.icndb.com',
		}),
		before(app) {
			app.use(`${publicPath}api`, jsonServer.router(DB_PATH));
		},
	};
};

function proxy({ path, target }) {
	const pathRewrite = { [`^${path}`]: '/' };
	return { [path]: { target, pathRewrite, changeOrigin: true } };
}
