const path = require('path');
const { DefinePlugin } = require('webpack');
const { merge } = require('webpack-merge');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const { jsFilename } = require('./hashed-asset.config');
const baseConfig = require('./webpack.base.config');
const DevServerConfig = require('./dev-server.config');
const { config } = require('../package');

const publicPath = process.env.PUBLIC_PATH || config.default_public_path;
const distPath = path.resolve(__dirname, '../dist');

// This config is concerned about the project in general
module.exports = merge(baseConfig, {
	entry: {
		app: './src/main.js',
	},
	output: {
		path: distPath,
		publicPath,
		filename: jsFilename,
	},
	// Preemptively block polyfilling Node.js globals.
	// This option will we removed in Webpack 5 anyway, plus is saves 4-5KB of bundle size.
	node: false,
	optimization: {
		moduleIds: 'hashed',
		// The runtime chunk is used when loading chunks of code split by Webpack
		runtimeChunk: 'single',
		// Make sure the vendor code (AKA node_modules) is split from app code for better caching
		splitChunks: { chunks: 'all' },
	},
	plugins: [
		new CleanWebpackPlugin(),
		// Allow client code to access publicPath via process.env.PUBLIC_PATH as a constant at build time
		new DefinePlugin({
			'process.env.PUBLIC_PATH': JSON.stringify(publicPath),
		}),
		// Used to check what ends up in the bundle
		...conditionally(process.env.ANALYZE, new BundleAnalyzerPlugin({
			analyzerMode: 'static',
			generateStatsFile: true,
		})),
	],
	performance: {
		maxEntrypointSize: 512000,
		maxAssetSize: 512000,
	},
	// Make sure Webpack runs in production mode when in doubt
	mode: process.env.NODE_ENV === 'development' ? 'development' : 'production',
	devServer: DevServerConfig({ publicPath }),
});

function conditionally(condition, value) {
	return condition ? [].concat(value) : [];
}
