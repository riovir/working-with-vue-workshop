import App from 'src/the-app';
import { mountComponent } from 'test/utils';

const JOKE_RESPONSES = {
	'joke-api/jokes/random': Joke('Joke with weird text'),
	'joke-api/jokes/random?escape=javascript': Joke('Proper joke'),
};

test('is testable with only the network mocked out', async () => {
	const { vm } = mountWith({ get: JOKE_RESPONSES });
	vm.$refs.theButton.click();
	vm.$refs.theButton.click();
	await vm.$nextTick();
	expect(vm.$refs.subtitle.textContent).toBe('You clicked Bulma 2 times');
	vm.$refs.theButton.click();
	vm.$refs.theButton.click();
	vm.$refs.theButton.click();
	await vm.$nextTick();
	/*
		Notice that multiple $refs are nested in each other.
		Use such a practice sparingly, never in unit tests.
		The more deep assertions in a test suite, the more fragile it tends to become.
	*/
	expect(vm.$refs.theMessage.$refs.modalMessage.textContent).toBe('Getting tired with all this incrementing nonsense.');
	vm.$refs.theMessage.$refs.showJoke.click();
	await vm.$nextTick(); // Request goes out
	await vm.$nextTick(); // Request arrives
	await vm.$nextTick(); // Modal state refreshes
	expect(vm.$refs.theMessage.$refs.modalMessage.textContent).toBe('Proper joke');
});

function mountWith(network) {
	const { axios, ...rest } = mountComponent(App);
	axios.get.mockImplementation(async url => network.get[url] || NoJoke());
	return { axios, ...rest };
}

async function Joke(joke) {
	return { data: { value: { joke } } };
}

async function NoJoke() {
	throw new Error('No joke for you');
}
