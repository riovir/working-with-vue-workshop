import { setupStore } from 'test/utils';

test('is integration testable with http client mock', async () => {
	const { store, axios } = setupStore();
	axios.get.mockResolvedValue({ data: Joke('Remote joke') });
	await store.dispatch('loadJoke');
	expect(axios.get).toHaveBeenCalledTimes(1);
	expect(axios.get).toHaveBeenCalledWith(expect.stringContaining('/jokes/random'));
	expect(axios.get).toHaveBeenCalledWith(expect.stringContaining('?escape=javascript'));
	expect(store.state.message).toBe('Remote joke');
});

function Joke(joke) {
	return { value: { joke } };
}
