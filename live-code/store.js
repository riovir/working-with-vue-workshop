export function Store({ api }) {
	const state = {
		results: [],
	};
	const actions = {
		async loadResults() {
			state.results = await api.fetchResults();
		},
	};
	return { state, actions };
}
