import { Vue } from './deps.js';
import { test, expect } from './poor-man/jest.js';
import TheApp from './the-app.js';
import { Store } from './store.js';

const FAILED_RESULT = { description: 'goes boom', outcome: 'boom' };

test('TheApp: renders', () => {
	const { vm } = setup();
	expect(vm.$el.textContent).toMatch(/TheApp/);
});

test('TheApp: loads results', async () => {
	const results = [{ description: 'mock', outcome: 'boom' }];
	const { vm, mountAsync } = setup({ results });
	await mountAsync();
	expect(vm.$el.textContent).toMatch(/mock/);
	expect(vm.$el.textContent).toMatch(/boom/);
});

function setup({
	fail = false,
	results = fail ? [FAILED_RESULT] : [],
	api = MockApi({ results }),
	store = Store({ api }),
} = {}) {
	const Constructor = Vue.extend(TheApp);
	const vm = new Constructor({ store }).$mount();
	const mountAsync = () => TheApp.mounted.call(vm);
	return { vm, mountAsync };
}

function MockApi({ results }) {
	const fetchResults = async () => results;
	return { fetchResults };
}
