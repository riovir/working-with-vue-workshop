import TheNotifications from './components/the-notifications.js';

export default {
	template: `
		<div class="container section">
			<h1 class="title is-1">TheApp</h1>

			<the-notifications />

			<v-details v-for="result in results" :key="result.description" v-bind="result" />
		</div>
	`,
	components: { TheNotifications },
	data() {
		return this.$store.state;
	},
	async mounted() {
		return this.$store.actions.loadResults();
	},
};
