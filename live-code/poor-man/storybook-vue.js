export function storybookOf(...widgets) {
	const components = widgets.map(storiesOf);
	return {
		functional: true,
		render: h => h('div', { class: 'container section' }, [
			h('h1', { class: 'title is-1' }, 'Storybook'),
			...components.map(h),
		]),
	};
}

/** "fun" fact: you need to alias a prop called "default" */
function storiesOf({ default: meta, ...stories }) {
	const components = Object.entries(stories).map(toStoryComponent);
	return {
		render: h => h('div', [
			h('h2', { class: 'title is-2' }, meta.title),
			...components.map(h),
		]),
	};
}

function toStoryComponent([title, Story]) {
	const StoryContent = { template: Story() };
	return {
		template: `
			<div class="box">
				<h2 class="subtitle">${title}</h2>
				<story-content></story-content>
			</div>
		`,
		components: { StoryContent },
	};
}
