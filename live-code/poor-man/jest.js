const results = []; // <- This is mutable global state. Renders this file untestable.

const eventuallyReportSummary = debounce(reportSummary, 1000);

/** Test flow */
export async function test(description, testCase) {
		try {
				await testCase();
				reportResult({ description, outcome: '✔️' });
		}
		catch({ message, details = [message] }) {
				reportResult({ description, outcome: '❌', details, logWith: console.error });
		}
		finally {
				eventuallyReportSummary(results);
		}
}

/** Assertions */
export function expect(received) {
	const not = predicate => value => expected => !predicate(value)(expected);
	const equals = value => expected => value === expected;
	const matches = value => expected => expected.test(value);
	/** Functional, yes, but not an example of simple code. This workshop has other focus. */
	const check = received => (isExpected, toDetails) => expected => {
		if (!isExpected(received)(expected)) {
			const details = ['Expected', received, ...toDetails(expected)];
			throw Object.assign(new Error(), { details });
		}
	};

	const checkReceived = check(received);
	return {
		toEqual: checkReceived(equals, expected => ['to equal', expected]),
		toMatch: checkReceived(matches, expected => ['to match', String(expected)]),
		get not() {
			return {
				toEqual: checkReceived(not(equals), expected => ['not to equal', expected]),
				toMatch: checkReceived(not(matches), expected => ['not to match', String(expected)]),
			};
		}
	};
}

/** Reporting */
function reportResult({ description, outcome, details, logWith = console.info }) {
		results.push({ description, outcome, details });
		logWith.bind(console)(`${description}: ${outcome}. `, ...(details || []));
}

function reportSummary(results) {
		const jsonDump = JSON.stringify(results, null, 4);
		console.info('===| SUMMARY |===');
		console.info(jsonDump);
}

/** Utils */
/*
 * Delay executing the last call of a function until the "dust has settled".
 * Typically used to avoid overwhelming the server with typing events. */
function debounce(func, wait) {
		let timeout;

		return function executedFunction(...args) {
				const later = () => {
						timeout = null;
						func(...args);
				};

				clearTimeout(timeout);
				timeout = setTimeout(later, wait);
		};
};
