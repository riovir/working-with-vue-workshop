export default {
	template: `
		<div v-if="failureCount && !seen" class="notification is-danger">
			<button ref="dismissButton" class="delete" @click="dismiss"></button>
			There were {{ failureCount }} failed tests.
		</div>
	`,
	data() {
		return {
			state: this.$store.state,
			seen: false,
		}
	},
	computed: {
		failureCount() {
			return this.state.results.filter(isFailure).length;
		}
	},
	methods: {
		dismiss() {
			this.seen = true;
		}
	}
};

function isFailure({ outcome }) {
	return outcome !== '✔️';
}
