import { Vue } from '../deps.js';
import { test, expect } from '../poor-man/jest.js';
import TheNotifications from './the-notifications.js';
import { Store } from '../store.js';

const FAILED_RESULT = { description: 'goes boom', outcome: 'boom' };

test('TheNotifications: notifies on failures', async () => {
	const { vm } = setup({ fail: true });
	expect(vm.$el.textContent).toMatch(/1 failed/);
});

test('TheNotifications: has no notification by default', async () => {
	const { vm } = setup();
	expect(vm.$el.textContent).not.toMatch(/failed/);
});

test('TheNotifications: can dismiss notification', async () => {
	const { vm } = setup({ fail: true });
	vm.$refs.dismissButton.click();
	await vm.$nextTick();
	expect(vm.$el.textContent).not.toMatch(/failed/);
});

function setup({
	fail = false,
	results = fail ? [FAILED_RESULT] : [],
	store = Store({ api: null }),
} = {}) {
	store.state.results = results;
	const Constructor = Vue.extend(TheNotifications);
	const vm = new Constructor({ store }).$mount();
	return { vm };
}
