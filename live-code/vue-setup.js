import { Vue } from './deps.js';
import { Api } from './api.js';
import { Store } from './store.js';
import { Widgets } from './widgets/index.js';

Vue.use(Widgets);

Vue.mixin({
	beforeCreate() {
		this.$store = this.$options.store ?? this.$parent?.$store;
	},
});

export const store = Store({
	api: Api({ fetch }),
});
