export default {
	template: `
		<details class="details" :class="{ 'is-blank': isBlank }">
			<summary>{{ description }} {{ outcome }}</summary>
			<span v-for="(part, index) in details" :key="index"> {{ part }} </span>
		</details>
	`,
	name: 'VDetails',
	props: {
		description: { type: String, default: '' },
		outcome: { type: String, default: '' },
		details: { type: Array, default: () => [] },
	},
	computed: {
		isBlank() {
			return !this.details.length;
		},
	}
};
