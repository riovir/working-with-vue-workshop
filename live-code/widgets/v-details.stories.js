export default { title: 'widgets/v-details' };

export const byDefault = () => `
	<v-details />
`;

export const withoutDetails = () => `
	<v-details description="Test is fine" outcome="✔️" />
`;

export const withDetails = () => `
	<v-details
		description="Test is fine"
		outcome="❌"
		:details="['some', 'details about', 42]"
	/>
`;
