export function Api({ fetch }) {
	return {
		async fetchResults() {
			const response = await fetch('/backend/results.json');
			return await response.json();
		}
	};
};
