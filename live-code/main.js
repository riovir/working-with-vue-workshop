import { Vue } from './deps.js';
import TheApp from './the-app.js';
import * as vueSetup from './vue-setup.js';

import { storybookOf } from './poor-man/storybook-vue.js';
import * as detailsStories from './widgets/v-details.stories.js';

new Vue({
	el: '#storybook',
	...storybookOf(detailsStories),
});

new Vue({
	el: '#app',
	...vueSetup,
	render: h => h(TheApp),
});

import './the-app.spec.js';
import './components/the-notifications.spec.js';
