# Addressed topics

## Project setup
- **CDNs and Sub-Resource Integrity**
- **main partition**
- **app partition**

## Vue.js API
- **vue style guide**
- **data for component state**
- **lifecycle hooks**
- **v-for iteration and the key**
- **computed values**
- **event handlers**
- **v-if for conditional block**
- **dependency injection with mixins**
- **templates and render functions**
- **props types and defaults**
- **props-in** events-out
- **object prop anti-pattern**
- **function prop anti-pattern
- **watch for imperative APIs**

## Vue ecosystem
- **Vuex**
- **VueRouter**
- **VueI18n**
- **FontAwesome: Vue edition
- **Vue and Webcomponents
- **mentioning Buefy and accessibility**
- **mentioning VueFocusLock**

## Patterns
- **Deno-like CDN imports: deps.js**
- **setup pattern**
- **RO-RO pattern**
- **Humble Object pattern**
- **factories over classes**
- **Widgets vs Application Components**
- **widget styles**
- **Domain Object Modules**

## Methodologies
- **JSON for rapid prototyping APIs**
- **specs for logic**
- **stories for presentation**
